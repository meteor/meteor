import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

// load custom components
import DisplayNode from './components/DisplayNode.vue'
import DisplayChannel from './components/DisplayChannel.vue'
import FormSchema from './components/FormSchema.vue'
import DeviceForm from './components/DeviceForm.vue'
import ChannelForm from './components/ChannelForm.vue'

import App from './App.vue'

// install Element-UI
Vue.use(ElementUI, { size: 'small' })

// install globally cutom components
Vue.component('display-node', DisplayNode)
Vue.component('display-channel', DisplayChannel)
Vue.component('form-schema', FormSchema)
Vue.component('device-form', DeviceForm)
Vue.component('channel-form', ChannelForm)

new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})

// prevent back page action which would result in loosing work done on meteor.
history.pushState(null, null, location.href)
window.onpopstate = function () {
    history.go(1)
}
window.onbeforeunload = function(ev) {
  let msg = 'Are you sure you want to leave ? All your work will be lost !'
  ev.returnValue = msg
  return msg
}
