let cloneObject = (s) => {
  let clone = {}
  for (let [k, v] of Object.entries(s)) {
    if (v instanceof Array) {
      clone[k] = []
      for (let o of v) {
        if (o instanceof Object) {
          clone[k].push(cloneObject(o))
        } else {
          clone[k].push(o)
        }
      }
    } else if (v instanceof Object) {
      clone[k] = cloneObject(v)
    } else {
      clone[k] = v
    }
  }
  return clone
}

let pzContent2DeviceInventory = (pz) => {
  let sortByLabel = (a, b) => {
    a = a.label
    b = b.label
    return a == b ? 0 : a < b ? -1 : 1
  }

  let recursiveSort = (x) => {
    x.sort(sortByLabel)
    for (let y of x) {
      if (y.children) {
        recursiveSort(y.children)
      }
    }
  }

  // process analog filters
  let analogFilters = { label: 'Analog filter', value: 'analog_filter', children: [] }
  for (let [manufacturer, models] of Object.entries(pz.analog_filter)) {
    let man = { label: manufacturer, value: manufacturer, children: [] }
    analogFilters.children.push(man)
    for (let [model, obj] of Object.entries(models)) {
      man.children.push({ label: model, value: model, data: obj })
    }
  }
  recursiveSort(analogFilters.children)

  // process digital filters
  let digitalFilters = { label: 'Digital filter', value: 'digital_filter', children: [] }
  for (let [manufacturer, models] of Object.entries(pz.digital_filter)) {
    let man = { label: manufacturer, value: manufacturer, children: [] }
    digitalFilters.children.push(man)
    for (let [model, obj] of Object.entries(models)) {
      man.children.push({ label: model, value: model, data: obj })
    }
  }
  recursiveSort(digitalFilters.children)

  let digitalSensorMap = {}
  // process digitizer
  let digitizer = { label: 'Digitizer', value: 'digitizer', children: [] }
  for (let [manufacturer, models] of Object.entries(pz.digitizer)) {
    let man = { label: manufacturer, value: manufacturer, children: [] }
    for (let [model, obj] of Object.entries(models)) {
      obj.component.sort()
      if (pz.sensor[manufacturer] && pz.sensor[manufacturer][model]) {
        // digital sensor detected
        if (!digitalSensorMap[manufacturer]) {digitalSensorMap[manufacturer] = {}}
        digitalSensorMap[manufacturer][model] = {
          serial_number: obj.serial_number,
          digitizer_component: obj.component,
          digitizer_analog_filter: obj.analog_filter,
          digitizer_digital_filter: obj.digital_filter,
          digitizer_configuration: obj.configuration
        }
      } else {
        man.children.push({ label: model, value: model, data: obj })
      }
    }
    if (man.children.length > 0) {digitizer.children.push(man)}
  }
  recursiveSort(digitizer.children)

  // process sensor
  let sensor = { label: 'Sensor', value: 'sensor', children: [] }
  for (let [manufacturer, models] of Object.entries(pz.sensor)) {
    let man = { label: manufacturer, value: manufacturer, children: [] }
    for (let [model, obj] of Object.entries(models)) {
      if (digitalSensorMap[manufacturer] && digitalSensorMap[manufacturer][model]) {
        // digital sensor detected
        Object.assign(digitalSensorMap[manufacturer][model], {
          sensor_component: obj.component,
          sensor_analog_filter: obj.analog_filter,
          sensor_configuration: obj.configuration,
          sensor_description: obj.description,
          sensor_period: obj.period,
          sensor_type: obj.type
        })
      } else {
        man.children.push({ label: model, value: model, data: obj })
      }
    }
    sensor.children.push(man)
  }
  recursiveSort(sensor.children)

  // finally restructurate digital filter
  let digitalSensor = { label: 'Digital sensor', value: 'digital_sensor', children: [] }
  for (let [manufacturer, models] of Object.entries(digitalSensorMap)) {
    let man = { label: manufacturer, value: manufacturer, children: [] }
    digitalSensor.children.push(man)
    for (let [model, obj] of Object.entries(models)) {
      man.children.push({ label: model, value: model, data: obj })
    }
  }
  recursiveSort(digitalSensor.children)

  return [
    sensor,
    digitalSensor,
    analogFilters,
    digitizer,
    digitalFilters
  ]
}

export default {
  cloneObject,
  pzContent2DeviceInventory
}
