{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "title": "Location",
  "description": "",
  "additionalProperties": true,
  "required": ["code", "lat_value", "lon_value", "alt_value", "depth", "vault", "geology"],
  "properties": {
    "code": {
      "type": "string",
      "title": "Location code",
      "maxLength": 2,
      "default": "00"
    },
    "latitude": {
      "title": "Latitude",
      "type": "object",
      "properties": {
        "lat_value": {
          "title": "value",
          "type": "number",
          "minimum": -90,
          "maximum": 90
        },
        "lat_min_err": {
          "title": "min err",
          "type": "number"
        },
        "lat_max_err": {
          "title": "max err",
          "type": "number"
        }
      }
    },
    "longitude": {
      "title": "Longitude",
      "type": "object",
      "properties": {
        "lon_value": {
          "title": "value",
          "type": "number",
          "minimum": -180,
          "maximum": 180
        },
        "lon_min_err": {
          "title": "min err",
          "type": "number"
        },
        "lon_max_err": {
          "title": "max err",
          "type": "number"
        }
      }
    },
    "altitude": {
      "title": "Altitude",
      "type": "object",
      "properties": {
        "alt_value": {
          "title": "value",
          "type": "number"
        },
        "alt_min_err": {
          "title": "min err",
          "type": "number"
        },
        "alt_max_err": {
          "title": "max err",
          "type": "number"
        }
      }
    },
    "depth": {
      "type": "number",
      "title": "Depth",
      "default": 0,
      "minimum": 0
    },
    "vault": {
      "title": "Vault",
      "default": "unknown",
      "enum": [
        {"key": "borehole", "label": "Borehole"},
        {"key": "cave", "label": "Cave"},
        {"key": "no_occ_build_gf", "label": "Unoccupied building (ground floor)"},
        {"key": "no_occ_build_u", "label": "Unoccupied building (underground)"},
        {"key": "no_occ_build_us", "label": "Unoccupied building (upper story)"},
        {"key": "occ_build_gf", "label": "Occupied building (ground floor)"},
        {"key": "occ_build_u", "label": "Occupied building (underground)"},
        {"key": "occ_build_us", "label": "Occupied building (upper story)"},
        {"key": "out", "label": "Outdoor site"},
        {"key": "out_bur", "label": "Outdoor site - buried"},
        {"key": "sea", "label": "Seafloor"},
        {"key": "sea_bur", "label": "Seafloor - buried"},
        {"key": "seis_borehole", "label": "Seismic borehole vault (or Borehole Vault)"},
        {"key": "shelter", "label": "Shelter/cabin"},
        {"key": "tun", "label": "Tunnel"},
        {"key": "under_gal", "label": "Underground gallery"},
        {"key": "unknown", "label": "Unknown vault"}
      ]
    },
    "geology": {
      "title": "Geology",
      "default": "unknown",
      "enum": [
        {"label": "A", "key": "A", "description": "rock or stiff geological formation (Vs30>800 m/s)"},
        {"label": "B", "key": "B", "description": "stiff deposits of sand, gravel or overconsolidated clays (Vs30>360m/s and Vs30<800 m/s)"},
        {"label": "BHd", "key": "BHd", "description": "Borehole sensors. d=depth in meters"},
        {"label": "Bm.n", "key": "Bm.n", "description": "Sensors in building (m=total number of stories of the building including ground floor, n=floor where the instrument is installed)"},
        {"label": "C", "key": "C", "description": "deep deposit of medium dense sand, gravel or medium stiff clays (Vs30>180m/s and Vs30<360 m/s)"},
        {"label": "D", "key": "D", "description": "loose cohesionless soil deposits (Vs30<180 m/s)"},
        {"label": "E", "key": "E", "description": "soil made up of superficial alluvial layer, with a thickness ranging from 5 to 20m with Vs30 value in class C and D ranges covering stiffer deposits (class A)"},
        {"label": "O", "key": "O", "description": "other"},
        {"label": "R", "key": "R", "description": "rock based on surface geological observations"},
        {"label": "S", "key": "S", "description": "sediment based on surface geological observations"},
        {"label": "S1", "key": "S1", "description": "deposits consisting of a level of soft clays with a thickness of at least 10m, with a high plasticity indices and a high water content (Vs30<100m/s)"},
        {"label": "S2", "key": "S2", "description": "liquefiable soils and soil profiles not included in the soil classes A-E or S1"},
        {"label": "unknown", "key": "unknown", "label": "unknown"}
      ],
      "pattern": "^(A|B|(BH[0-9]+)|(B[0-9]+\\.[0-9]+)|C|D|E|O|R|S|S1|S2|unknown)$"
    }
  }
}
