{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Meteor station response description schema",

  "definitions": {

    "network_values": {
      "type": "object",
      "additionalProperties": false,
      "required": ["fdsn", "open"],
      "properties": {
        "fdsn": {"type": "string"},
        "description": {"type": "string"},
        "contact": {"type": "string", "format": "email"},
        "open": {"type": "string", "format": "date-time"},
        "close": {"type": "string", "format": "date-time"},
        "alternate": {"type": "string"}
      }
    },
    "network": {
      "type": "object",

      "required": ["id", "label", "nodeType", "values"],
      "properties": {
        "id": { "type": "integer" },
        "label": {"type": "string"},
        "nodeType": {"enum": ["network"]},
        "values": {"$ref": "#/definitions/network_values"}
      }
    },
    "network_collection": {
      "type": "object",
      "additionalProperties": false,
      "required": ["id", "label", "nodeType", "addNode", "children"],
      "properties": {
        "id": { "type": "integer" },
        "label": {"type": "string"},
        "addNode": {"enum": [true]},
        "nodeType": {"enum": ["networkCollection"]},
        "children": {"type": "array", "items": {"$ref": "#/definitions/network"}}
      }
    },

    "location_values": {
      "type": "object",
      "additionalProperties": false,
      "required": ["code", "lat_value", "lon_value", "alt_value", "depth"],
      "properties": {
        "code": {"type": "string"},
        "lat_value": {"type": "number"},
        "lat_min_err": {"type": "number"},
        "lat_max_err": {"type": "number"},
        "lon_value": {"type": "number"},
        "lon_min_err": {"type": "number"},
        "lon_max_err": {"type": "number"},
        "alt_value": {"type": "number"},
        "alt_min_err": {"type": "number"},
        "alt_max_err": {"type": "number"},
        "depth": {"type": "number"},
        "vault": {"type": "string"},
        "geology": {"type": "string"}
      }
    },
    "location": {
      "type": "object",
      "additionalProperties": false,
      "required": ["id", "label", "nodeType", "values"],
      "properties": {
        "id": { "type": "integer" },
        "label": {"type": "string"},
        "nodeType": {"enum": ["location"]},
        "values": {"$ref": "#/definitions/location_values"}
      }
    },
    "location_collection": {
      "type": "object",
      "additionalProperties": false,
      "required": ["id", "label", "addNode", "nodeType", "children"],
      "properties": {
        "id": { "type": "integer" },
        "label": {"type": "string"},
        "addNode": {"enum": [true]},
        "nodeType": {"enum": ["locationCollection"]},
        "children": {"type": "array", "items": {"$ref": "#/definitions/location"}}
      }
    },

    "channel": {
      "type": "object",
      "additionalProperties": false,
      "required": ["iris_code", "azimuth", "dip"],
      "properties": {
        "iris_code": {"type": "string"},

        "sensor_component": {"type": "string"},
        "sensor_configuration": {"type": "string"},
        "sensor_analog_filter": {"type": "string"},

        "analog_filter_component": {"type": "string"},

        "digitizer_component": {"type": "string"},
        "digitizer_configuration": {"type": "string"},
        "digitizer_analog_filter": {"type": "string"},
        "digitizer_digital_filter": {"type": "string"},

        "digital_filter_filter": {"type": "string"},

        "digital_sensor_sensor_component": {"type": "string"},
        "digital_sensor_sensor_configuration": {"type": "string"},
        "digital_sensor_sensor_analog_filter": {"type": "string"},

        "digital_sensor_digitizer_component": {"type": "string"},
        "digital_sensor_digitizer_configuration": {"type": "string"},
        "digital_sensor_digitizer_analog_filter": {"type": "string"},
        "digital_sensor_digitizer_digital_filter": {"type": "string"},

        "software_filter": {"type": "string"},

        "azimuth": {"type": "number"},
        "dip": {"type": "number"}
      }
    },
    "device": {
      "type": "object",
      "additionalProperties": false,
      "required": ["device", "serial"],
      "properties": {
        "device": {
          "type": "array",
          "items": [
            {"type": "string"},
            {"type": "string"},
            {"type": "string"}
          ]
        },
        "serial": {"type": "string"}
      }
    },
    "channel_group_values": {
      "type": "object",
      "additionalProperties": false,
      "required": ["network", "location", "format", "flags", "open"],
      "properties": {
        "device": {"type": "array", "items": {"$ref": "#/definitions/device"}},
        "channel": {"type": "array", "items": {"$ref": "#/definitions/channel"}},
        "network": {"type": "integer"},
        "location": {"type": "integer"},
        "flags": {"type": "array", "items": {"enum": ["T", "C", "H", "G", "W"]}},
        "format": {"enum":  ["ASCII", "INT16", "INT24", "INT32",
                             "IEEEFLOAT", "IEEEDOUBLE", "STEIM1",  "STEIM2",
                             "GeoscopeMult24", "GeoscopeMult16E3", "GeoscopeMult16E4",
                             "USNnc", "CDSN16", "GRAEFENBERG", "IPG16", "STEIM3", "SRO",
                             "HGLP", "DWWSSN", "RSTN16"]},
        "open": {"type": "string", "format": "date-time"},
        "close": {"type": "string", "format": "date-time"}
      }
    },
    "channel_group": {
      "type": "object",
      "additionalProperties": false,
      "required": ["id", "label", "nodeType", "values"],
      "properties": {
        "id": { "type": "integer" },
        "label": {"type": "string"},
        "nodeType": {"enum": ["channelGroup"]},
        "values": {"$ref": "#/definitions/channel_group_values"}
      }
    },
    "channel_group_collection": {
      "type": "object",
      "additionalProperties": false,
      "required": ["id", "label", "addNode", "nodeType", "children"],
      "properties": {
        "id": { "type": "integer" },
        "label": {"type": "string"},
        "addNode": {"enum": [true]},
        "nodeType": {"enum": ["channelGroupCollection"]},
        "children": {"type": "array", "items": {"$ref": "#/definitions/channel_group"}}
      }
    },
    "station_values": {
      "type": "object",
      "additionalProperties": false,
      "required": ["iris_code", "first_install", "lat_value", "lon_value", "alt_value"],
      "properties": {
        "iris_code": {"type": "string"},
        "toponyme":  {"type": "string"},
        "contact":   {"type": "string"},
        "owner":   {"type": "string"},
        "first_install":   {"type": "string", "format": "date-time"},
        "close":   {"type": "string", "format": "date-time"},
        "lat_value": {"type": "number"},
        "lat_min_err": {"type": "number"},
        "lat_max_err": {"type": "number"},
        "lon_value": {"type": "number"},
        "lon_min_err": {"type": "number"},
        "lon_max_err": {"type": "number"},
        "alt_value": {"type": "number"},
        "alt_min_err": {"type": "number"},
        "alt_max_err": {"type": "number"}
      }
    },
    "station": {
      "type": "object",
      "additionalProperties": false,
      "required": ["id", "label", "nodeType", "values"],
      "properties": {
        "id": { "type": "integer" },
        "label": {"type": "string"},
        "nodeType": {"enum": ["station"]},
        "values": {"$ref": "#/definitions/station_values"},
        "children": {
          "type": "array",
          "items": {
            "oneOf": [
                      {"$ref": "#/definitions/location_collection"},
                      {"$ref": "#/definitions/channel_group_collection"}
                     ]
          }
        }
      }
    },
    "station_collection": {
      "type": "object",
      "additionalProperties": false,
      "required": ["id", "label", "addNode", "nodeType", "children"],
      "properties": {
        "id": { "type": "integer" },
        "label": {"type": "string"},
        "addNode": {"enum": [true]},
        "nodeType": {"enum": ["stationCollection"]},
        "children": {"type": "array", "items": {"$ref": "#/definitions/station"}}
      }

    }
  },

  "type": "array",
  "items":  [
    {"$ref": "#definitions/network_collection"},
    {"$ref": "#definitions/station_collection"}
  ]
}
