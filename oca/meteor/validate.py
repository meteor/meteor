# -*- coding: utf-8 -*-
import os
import json
import jsonschema

METEOR_SCHEMA = os.path.join(os.path.dirname(__file__), 'meteor-schema.json')


class JsonMeteorValidator(object):

    def __init_valid_network(self, networks):
        """
        Initialize the set of all network meteor id.
        This method verify that network meteor id is unique.

        networks -- The list of meteor network
        """
        self.__valid_network = set()
        # Get all valid network meteor id
        for current_network in networks:
            if current_network["id"] in self.__valid_network:
                raise ValueError("Network with meteor id %s already declared" %
                                 current_network["id"])
            self.__valid_network.add(current_network["id"])

    def __update_declared_station(self, iris_code):
        """
        Update already declared station set with a given IRIS code
        This method verify that the given code is not already present

        iris_code -- The new IRIS station code
        """
        if iris_code in self.__already_declared_station:
            raise ValueError("Station %s appear twice" % iris_code)
        self.__already_declared_station.add(iris_code)

    def __init_valid_location(self, locations):
        self.__valid_location = set()
        for current_location in locations:
            if current_location["id"] in self.__valid_location:
                raise ValueError("Location with meteor id %s already declared" %
                                 current_location["id"])
            self.__valid_location.add(current_location["id"])

    def __check_json_structure(self, json_meteor):
        """
        Check json meteor structure with meteor json schema.

        json_meteor -- The json meteor to check
        """
        schema_file = open(METEOR_SCHEMA)
        schema = json.load(schema_file)
        jsonschema.validate(json_meteor, schema)

    def __check_channel(self, channel):
        """
        Check channel validity.
          -- Check that sensor component is present
          -- Check that digitizer component is present

        channel       -- The meteor channel
        """
        # Check sensor component is present
        if("sensor_component" not in channel and
           "digital_sensor_sensor_component" not in channel):
            raise ValueError("Can't find sensor component for channel group %s of station %s" %
                             (self.__current_channel_group["label"],
                              self.__current_station["values"]["iris_code"]))

        # Check digitizer component is present
        if("digitizer_component" not in channel and
           "digital_sensor_digitizer_component" not in channel):
            raise ValueError("Can't find digitizer component for channel group %s of station %s" %
                             (self.__current_channel_group["label"],
                              self.__current_station["values"]["iris_code"]))

    def __check_channel_group(self, channel_group):
        """
        Check channel group validity.
          -- Check that network and location are valid.
          -- Check validity of all channel present in this group

        channel_group -- The meteor channel group
        """
        # Update current channel group
        self.__current_channel_group = channel_group

        # Check network existence
        if channel_group["values"]["network"] not in self.__valid_network:
            raise ValueError("Invalid network for channel group %s of station %s" %
                             (channel_group["label"],
                              self.__current_station["values"]["iris_code"]))
        # Check location existence
        if channel_group["values"]["location"] not in self.__valid_location:
            raise ValueError("Invalid location for channel group %s of station %s" %
                             (channel_group["label"],
                              self.__current_station["values"]["iris_code"]))
        # Check channels validity
        for current_channel in channel_group["values"]["channel"]:
            self.__check_channel(current_channel)

    def __check_station(self, station):
        """
        Check station validity.
          -- Check that station with same IRIS code is not already declared
          -- Check validity of all channel group belonging to this station

        station       -- The meteor station
        """
        # Update current station
        self.__current_station = station

        # Update declared station set
        self.__update_declared_station(station["values"]["iris_code"])

        # Initialize valid location meteor id for station
        self.__init_valid_location(station["children"][0]["children"])

        # Check station channel group
        for current_channel_group in station["children"][1]["children"]:
            self.__check_channel_group(current_channel_group)

    def check_json_meteor(self, json_meteor):
        """
        Validate a given json_meteor with meteor schema and
        make various coherence checks.

        json_meteor -- The json meteor to check.

        Can raise:
              jsonschema.SchemaError     -- If the meteor schema is invalid
              jsonschema.ValidationError -- If the json meteor can't be validate by schema
              ValueError                 -- If json meteor is not coherent
        """
        # Reset already declared station
        self.__already_declared_station = set()

        # Validate json meteor with schema
        self.__check_json_structure(json_meteor)

        # Initialize valid network meteor id set
        self.__init_valid_network(json_meteor[0]["children"])

        # Check station coherence
        for current_station in json_meteor[1]["children"]:
            self.__check_station(current_station)
