# -*- coding: utf-8 -*-
from threading import Lock
from datetime import datetime
from pzbank.loader import PZAccess
from collections import defaultdict


PZACCESS = None


class DbirdIndex(object):
    """
    Class for managing index in each station.
    """
    def __init__(self):
        self.__index_table = defaultdict(lambda: defaultdict(lambda: 1))

    def next_index(self, station_code, stage_type):
        """
        Give the next index for a given station and stage type.

        station_code -- The iris code of the station
        stage_type   -- The stage type as string

        return       -- The next index
        """
        result = self.__index_table[station_code][stage_type]
        self.__index_table[station_code][stage_type] += 1
        return result


# Global lock used to prevent concurrent generation
generator_lock = Lock()

# Global index generator
dbird_index_generator = DbirdIndex()

# The current processed station
# Used to generate per station dbird index
current_station = None


def next_dbird_index(station_code, stage_type):
    """
    Give the next index for a given station_code and stage_type.

    station_code -- The station iris code
    stage_type   -- The stage type as string

    return       -- The next index
    """
    return dbird_index_generator.next_index(station_code, stage_type)


class Dbird(object):

    def __init__(self, meteor_data, pz_access):
        """
        Initialize this Dbird

        meteor_data -- The array of dictionnary describing stations responses
        pz_access   -- The PZAccess databank loader
        """
        generator_lock.acquire()
        try:
            # First reset dbird index generator
            global dbird_index_generator
            global PZACCESS

            dbird_index_generator = DbirdIndex()
            PZACCESS = pz_access
            self.__authors = list()
            self.__networks = list()
            self.__stations = list()

            # Load networks
            for current_network in meteor_data[0]["children"]:
                self.__networks.append(DbirdNetwork(current_network))

            # Load stations
            for current_station in meteor_data[1]["children"]:
                self.__stations.append(DbirdStation(self, current_station))
        finally:
            generator_lock.release()

    def get_network(self, meteor_id):
        """
        Give the DbirdNetwork with given meteor id

        meteor_id -- The meteor id

        result    -- The DbirdNetwork with given meteor id
        """
        for current in self.__networks:
            if current.meteor_id() == meteor_id:
                return current
        raise ValueError("Can't find DbirdNetwork with meteor id %s" %
                         meteor_id)

    def authors(self):
        """
        Give the list of authors
        """
        return self.__authors

    def networks(self):
        """
        Give the list of networks
        """
        return self.__networks

    def stations(self):
        """
        Give the list of stations
        """
        return self.__stations


class DbirdObject(object):

    def __init__(self, dbird_id):
        """
        Initialized this DbirdObject

        dbird_id -- The id of this DbirdObject
        """
        self.__dbird_id = dbird_id

    def dbird_id(self):
        """
        Give the dbird id of this DbirdObject
        """
        return self.__dbird_id


class DbirdAuthor(DbirdObject):

    def __init__(self, definition):
        index = next_dbird_index("NOSTATION", "AUTHOR")
        dbird_id = "AUTH%s" % index
        DbirdObject.__init__(self, dbird_id)
        self.__definition = definition

    def firstname(self):
        """
        Give the firstname of this DbirdAuthor
        """
        return self.__definition["firstname"]

    def lastname(self):
        """
        Give the lastname of this DbirdAuthor
        """
        return self.__definition["lastname"]

    def emails(self):
        """
        Give the mail list of this DbirdAuthor
        """
        return self.__definition["emails"]

    def phones(self):
        """
        Give the phones list of this DbirdAuthor
        """
        return self.__definition["phones"]


class DbirdComment(DbirdObject):
    def __init__(self, dbird_id, definition):
        DbirdObject.__init__(self, dbird_id)
        self.__definition = definition

    def value(self):
        """
        Give the message of this DbirdComment
        """
        return self.__definition["value"]

    def begin_date(self):
        """
        Give the begin validity date of this DbirdComment

        return -- datetime.datetime
        """
        return self.__definition["begin_date"]

    def end_date(self):
        """
        Give the end validity date of this DbirdComment

        return -- datetime.datetime
        """
        return self.__definition.get("close_date", "present")

    def authors(self):
        """
        Give the DbirdAuthor list of this DbirdComment
        """
        return self.__definition["authors"]


class DbirdNetwork(DbirdObject):

    def __init__(self, definition):
        """
        Initialize this DbirdNetwork according to a given definition.

        definition -- The definition of this DbirdNetwork
        """
        index = next_dbird_index("NOSTATION", "NETWORK")
        dbird_id = "NET%s" % index
        DbirdObject.__init__(self, dbird_id)
        self.__definition = definition["values"]
        self.__meteor_id = definition["id"]

    def meteor_id(self):
        """
        Give the meteor id of this DbirdNetwork
        """
        return self.__meteor_id

    def comments(self):
        """
        Give the list of DbirdComment of this DbirdNetwork
        """
        return list()

    def fdsn(self):
        """
        Give the FDSN code of this DbirdNetwork
        """
        return self.__definition["fdsn"]

    def description(self):
        """
        Give the description of this DbirdNetwork
        """
        return self.__definition.get("description", "")

    def contact(self):
        """
        Give the contact mail of this DbirdNetwork
        """
        return self.__definition.get("contact", "")

    def open_date(self):
        """
        Give the open date of this DbirdNetwork

        return -- The datetime as string in ISO format
        """
        return self.__definition["open"]

    def close_date(self):
        """
        Give the close date of this DbirdNetwork

        return -- datetime.datetime
        """
        return self.__definition.get("close", "present")

    def alternate(self):
        """
        Give the alternate code of this DbirdNetwork
        """
        return self.__definition.get("alternate", "")


class DbirdLocation(DbirdObject):

    def __init__(self, definition):
        """
        Initialize this DbirdLocation according to given definition
        """
        global current_station
        index = next_dbird_index(current_station, "LOCATION")
        dbird_id = "LOC%s" % index
        DbirdObject.__init__(self, dbird_id)
        self.__definition = definition["values"]
        self.__meteor_id = definition["id"]

    def meteor_id(self):
        """
        Give the meteor id of this DbirdLocation
        """
        return self.__meteor_id

    def code(self):
        """
        Give the code of this DbirdLocation
        """
        return self.__definition["code"]

    def latitude(self):
        """
        Give the latitude of this DbirdLocation

        return -- A list like (<latitude value>, <latitude min error>,
                                <latitude plus error>)
        """
        min_err = float(self.__definition["lat_min_err"]) if "lat_min_err" in self.__definition else "none"
        max_err = float(self.__definition["lat_max_err"]) if "lat_max_err" in self.__definition else "none"
        return list([float(self.__definition["lat_value"]), min_err, max_err])

    def longitude(self):
        """
        Give the longitude of this DbirdLocation

        return -- A list like (<longitude value>, <longitude min error>,
                                <longitude plus error>)
        """
        min_err = float(self.__definition["lon_min_err"]) if "lon_min_err" in self.__definition else "none"
        max_err = float(self.__definition["lon_max_err"]) if "lon_max_err" in self.__definition else "none"
        return list([float(self.__definition["lon_value"]), min_err, max_err])

    def altitude(self):
        """
        Give the altitude of this DbirdLocation

        return -- A list like (<altitude value>, <altitude min error>,
                                <altitude plus error>)
        """
        min_err = float(self.__definition["alt_min_err"]) if "alt_min_err" in self.__definition else "none"
        max_err = float(self.__definition["alt_max_err"]) if "alt_max_err" in self.__definition else "none"
        return list([float(self.__definition["alt_value"]), min_err, max_err])

    def depth(self):
        """
        Give the depth of this DbirdLocation
        """
        return float(self.__definition["depth"])

    def vault(self):
        """
        Give the vault of this DbirdLocation
        """
        return self.__definition.get("vault", "")

    def geology(self):
        """
        Give the geology of this DbirdLocation
        """
        return self.__definition.get("geology", "")


class DbirdDeviceComponent(DbirdObject):
    def __init__(self, dbird_id, model, serial, pz_file):
        DbirdObject.__init__(self, dbird_id)
        self.__model = model
        self.__serial = serial
        self.__pz_file = pz_file

    def model(self):
        """
        Give the model of this DbirdDeviceComponent
        """
        return self.__model

    def serial(self):
        """
        Give the serial of this DbirdDeviceComponent
        """
        if self.__serial is None:
            return "unknown"
        return self.__serial

    def pz_file(self):
        """
        Give the PZ file of this DbirdDeviceComponent
        """
        return self.__pz_file


class DbirdSensorComponent(DbirdDeviceComponent):
    def __init__(self, model, serial, pz_file, azimuth, dip):
        global current_station
        index = next_dbird_index(current_station, "SENSOR")
        dbird_id = "SEN%s" % index
        DbirdDeviceComponent.__init__(self, dbird_id, model, serial, pz_file)
        self.__azimuth = azimuth
        self.__dip = dip

    def azimuth(self):
        """
        Give the azimuth of this DbirdSensorComponent
        """
        return float(self.__azimuth)

    def dip(self):
        """
        Give the dip of this DbirdSensorComponent
        """
        return float(self.__dip)


class DbirdAnalogFilterComponent(DbirdDeviceComponent):
    def __init__(self, model, serial, pz_file):
        global current_station
        index = next_dbird_index(current_station, "ANALOG_FILTER")
        dbird_id = "AF%s" % index
        DbirdDeviceComponent.__init__(self, dbird_id, model, serial, pz_file)


class DbirdDigitizerComponent(DbirdDeviceComponent):
    def __init__(self, model, serial, pz_file):
        global current_station
        index = next_dbird_index(current_station, "DIGITIZER")
        dbird_id = "DIG%s" % index
        DbirdDeviceComponent.__init__(self, dbird_id, model, serial, pz_file)


class DbirdDigitalFilterComponent(DbirdDeviceComponent):
    def __init__(self, model, serial, pz_file):
        global current_station
        index = next_dbird_index(current_station, "DIGITAL_FILTER")
        dbird_id = "DF%s" % index
        DbirdDeviceComponent.__init__(self, dbird_id, model, serial, pz_file)


class DbirdChannel(object):
    def __init__(self, definition):
        """
        Initialize this DbirdChannel

        definition -- The DbirdChannel definition
        """
        self.__definition = definition

    def location(self):
        """
        Give the dbird id of this DbirdChannel DbirdLocation
        """
        return self.__definition["location"]

    def iris_code(self):
        """
        Give the IRIS code of this DbirdChannel
        """
        return self.__definition["iris_code"]

    def sensor(self):
        """
        Give the dbird id of this DbirdChannel DbirdSensorComponent
        """
        return self.__definition["sensor"]

    def analog_filters(self):
        """
        Give the dbird id of this DbirdChannel DbirdAnalogFilterComponent list
        """
        return self.__definition["analog_filters"]

    def digitizer(self):
        """
        Give the dbird id of this DbirdChannel DbirdDigitizerComponent
        """
        return self.__definition["digitizer"]

    def digital_filters(self):
        """
        Give the dbird id of this DbirdChannel DbirdDigitalFilterComponent list
        """
        return self.__definition["digital_filters"]

    def flags(self):
        """
        Give the list of flags of this DbirdChannel
        """
        return self.__definition["flags"]

    def format(self):
        """
        Give the data format of this DbirdChannel
        """
        return self.__definition["format"]

    def open_date(self):
        """
        Give the open date of this DbirdChannel

        return -- datetime.datyetime
        """
        return self.__definition["open_date"]

    def close_date(self):
        """
        Give the close date of this DbirdChannel

        return -- datetime.datyetime
        """
        return self.__definition["close_date"]

    def network(self):
        """
        Give the dbird id of this DbirdChannel DbirdNetwork
        """
        return self.__definition["network"]

    def comments(self):
        """
        Give the list of DbirdComment of this DbirdChannel
        """
        return self.__definition["comments"]


class DbirdStation(object):
    """
    """

    def __init__(self, dbird, definition):
        """
        Initialize this DbirdStation according to given definition.

        dbird      -- The Dbird object creating this station
        definition -- The definition of this DbirdStation
        """
        global current_station

        current_station = definition["values"]["iris_code"]
        self.__dbird = dbird
        self.__definition = definition["values"]
        self.__comments = list()
        self.__locations = list()
        self.__sensors = list()
        self.__analog_filters = list()
        self.__digitizers = list()
        self.__digital_filters = list()
        self.__channels = list()
        # Create station locations
        for current_location in definition["children"][0]["children"]:
            self.__locations.append(DbirdLocation(current_location))

        # Create station channels
        for current_group in definition["children"][1]["children"]:
            self.__create_channel_group(current_group["values"])

    def __get_location(self, meteor_id):
        """
        Give the DbirdLocation associated with a given
        meteor id.

        meteor_id -- The location meteor id

        return    -- The DbirdLocation
        """
        for current in self.__locations:
            if current.meteor_id() == meteor_id:
                return current
        raise ValueError("Can't find DbirdLocation with meteor id %s" %
                         meteor_id)

    def __get_device_component(self, metatype, device,
                               configuration=None, component=None,
                               meteor_channel_group=None,
                               azimuth=None, dip=None):
        """
        Give dbird id of a device component.

        metatype             -- The device metatype
        device               -- The device description
        configuration        -- The device configuration or None
        component            -- The device component
        meteor_channel_group -- The meteor channel groupo definition
        azimuth              -- The component azimuth for sensor component
        dip                  -- The component dip for sensor component

        return               -- The DbirdDeviceComponent
        """
        def find_device_component(component_list, pz_file, azimuth, dip):
            for current in component_list:
                if current.pz_file() == pz_file:
                    return current

            # Component is not present create a new one
            if device_component_constructor == DbirdSensorComponent:
                new_component = device_component_constructor(device["device"][2], device["serial"], pz_file,
                                                             azimuth, dip)
            else:
                new_component = device_component_constructor(device["device"][2], device["serial"], pz_file)
            device_component_list.append(new_component)

            return new_component

        # 1994-01-01T00:00:00Z
        datetime_start_date = None
        datetime_end_date = None
        if meteor_channel_group is not None:
            datetime_start_date = datetime.strptime(meteor_channel_group["open"],
                                                    "%Y-%m-%dT%H:%M:%SZ")
            if meteor_channel_group["close"] is not None:
                datetime_end_date = datetime.strptime(meteor_channel_group["close"],
                                                      "%Y-%m-%dT%H:%M:%SZ")
        pz_file = PZACCESS.get_pz_file(metatype, device["device"][1], device["device"][2], configuration,
                                       device["serial"], component, datetime_start_date,
                                       datetime_end_date)
        if metatype == PZAccess.SENSOR_METATYPE:
            device_component_list = self.__sensors
            device_component_constructor = DbirdSensorComponent
        elif metatype == PZAccess.ANALOG_FILTER_METATYPE:
            device_component_list = self.__analog_filters
            device_component_constructor = DbirdAnalogFilterComponent
        elif metatype == PZAccess.DIGITIZER_METATYPE:
            device_component_list = self.__digitizers
            device_component_constructor = DbirdDigitizerComponent
        elif (metatype == PZAccess.DIGITAL_FILTER_METATYPE or
              metatype == PZAccess.SOFTWARE_DIGITAL_FILTER_METATYPE):
            device_component_list = self.__digital_filters
            device_component_constructor = DbirdDigitalFilterComponent
        else:
            raise ValueError("Unknown metatype %s" % metatype)

        result = find_device_component(device_component_list, pz_file,
                                       azimuth, dip)
        return result

    def __get_device_info(self, device_list, device_type):
        """
        Give the device definition according to a device
        definition list and a device type.

        device_list -- The device definition list
        device_type -- The type of requested device

        return      -- The device definition
        """
        for current in device_list:
            if current["device"][0] == device_type:
                serial = current["serial"] if current["serial"] not in ["theoretical", ""] else None
                current["serial"] = serial
                return current
        raise ValueError("Can't find device type %s in %s" %
                         (device_type, device_list))

    def __initialize_dbird_channel(self, channel_def, channel_group_def):
        """
        Initialized a dbird channel description.

        channel_def       -- The channel definition
        channel_group_def -- The channel group definition

        return            -- The initialized dbird channel description
        """
        close_date = None
        if "close" in channel_group_def and channel_group_def["close"] != "":
            close_date = channel_group_def["close"]
        return {
            "iris_code": channel_def["iris_code"],
            "network": self.__dbird.get_network(channel_group_def["network"]).dbird_id(),
            "location": self.__get_location(channel_group_def["location"]).dbird_id(),
            "analog_filters": list(),
            "digital_filters": list(),
            "flags": channel_group_def["flags"],
            "format": channel_group_def["format"],
            "open_date": channel_group_def["open"],
            "close_date": "present" if close_date is None else close_date,
            "comments": list()
        }

    def __update_sensor(self, dbird_channel, meteor_channel, meteor_channel_group):
        """
        Update sensor information of a given dbird channel according to given
        meteor information.

        dbird_channel        -- The dbird channel description
        meteor_channel       -- The meteor channel description
        meteor_channel_group -- The meteor channel group description
        """
        prefix = ""
        if "sensor_component" not in meteor_channel:
            prefix = "digital_sensor_"
            sensor = self.__get_device_info(meteor_channel_group["device"], "digital_sensor")
        else:
            sensor = self.__get_device_info(meteor_channel_group["device"], "sensor")
        sensor_configuration = None
        if "%ssensor_configuration" % prefix in meteor_channel:
            sensor_configuration = meteor_channel["%ssensor_configuration" % prefix]
        sensor_component = meteor_channel["%ssensor_component" % prefix]

        dbird_sensor_component = self.__get_device_component(PZAccess.SENSOR_METATYPE,
                                                             sensor,
                                                             sensor_configuration,
                                                             sensor_component,
                                                             meteor_channel_group,
                                                             meteor_channel["azimuth"],
                                                             meteor_channel["dip"])
        dbird_channel["sensor"] = dbird_sensor_component.dbird_id()

        if("%sensor_analog_filter" % prefix) in meteor_channel:
            dbird_sensor_analog_filter = self.__get_device_component(PZAccess.ANALOG_FILTER_METATYPE,
                                                                     sensor, sensor_configuration,
                                                                     sensor_component,
                                                                     meteor_channel_group)

            dbird_channel["analog_filters"].append(dbird_sensor_analog_filter.dbird_id())

    def __update_analog_filter(self, dbird_channel, meteor_channel, meteor_channel_group):
        """
        Update analog filter information of a given dbird channel according to given
        meteor information.

        dbird_channel        -- The dbird channel description
        meteor_channel       -- The meteor channel description
        meteor_channel_group -- The meteor channel group description
        """
        if "analog_filter_component" in meteor_channel:
            analog_filter = self.__get_device_info(meteor_channel_group["device"], "analog_filter")
            dbird_analog_filter = self.__get_device_component(PZAccess.ANALOG_FILTER_METATYPE,
                                                              analog_filter, None,
                                                              meteor_channel["analog_filter_component"],
                                                              meteor_channel_group)
            dbird_channel["analog_filters"].append(dbird_analog_filter.dbird_id())

    def __update_digitizer(self, dbird_channel, meteor_channel, meteor_channel_group):
        """
        Update digitizer information of a given dbird channel according to given
        meteor information.

        dbird_channel        -- The dbird channel description
        meteor_channel       -- The meteor channel description
        meteor_channel_group -- The meteor channel group description
        """
        if "digitizer_component" in meteor_channel:
            prefix = ""
            digitizer = self.__get_device_info(meteor_channel_group["device"],
                                               "digitizer")
        else:
            prefix = "digital_sensor_"
            digitizer = self.__get_device_info(meteor_channel_group["device"],
                                               "digital_sensor")
        digitizer_configuration = None
        if "%sdigitizer_configuration" % prefix in meteor_channel:
            digitizer_configuration = meteor_channel["%sdigitizer_configuration" % prefix]
        digitizer_component = meteor_channel["%sdigitizer_component" % prefix]
        dbird_digitizer = self.__get_device_component(PZAccess.DIGITIZER_METATYPE,
                                                      digitizer,
                                                      digitizer_configuration,
                                                      digitizer_component,
                                                      meteor_channel_group)
        dbird_channel["digitizer"] = dbird_digitizer.dbird_id()

        # Manage digitizer analog filter
        if ("%sdigitizer_analog_filter" % prefix) in meteor_channel:
            filter_name = meteor_channel["%sdigitizer_analog_filter" % prefix]
            dbird_digitizer_analog_filter = self.__get_device_component(PZAccess.ANALOG_FILTER_METATYPE,
                                                                        digitizer,
                                                                        filter_name,
                                                                        digitizer_component,
                                                                        meteor_channel_group)
            dbird_channel["analog_filters"].append(dbird_digitizer_analog_filter.dbird_id())

        # Manage digitizer digital filter
        if ("%sdigitizer_digital_filter" % prefix) in meteor_channel:
            filter_name = meteor_channel["%sdigitizer_digital_filter" % prefix]
            dbird_digitizer_digital_filter = self.__get_device_component(PZAccess.DIGITAL_FILTER_METATYPE,
                                                                         digitizer,
                                                                         filter_name)
            dbird_channel["digital_filters"].append(dbird_digitizer_digital_filter.dbird_id())

    def __update_digital_filter(self, dbird_channel, meteor_channel, meteor_channel_group):
        """
        Update digital filter information of a given dbird channel according to given
        meteor information.

        dbird_channel        -- The dbird channel description
        meteor_channel       -- The meteor channel description
        meteor_channel_group -- The meteor channel group description
        """
        if "digital_filter_filter" in meteor_channel:
            digital_filter = self.__get_device_info(meteor_channel_group["device"], "digital_filter")
            dbird_digital_filter = self.__get_device_component(PZAccess.DIGITAL_FILTER_METATYPE,
                                                               digital_filter,
                                                               meteor_channel["digital_filter_filter"])
            dbird_channel["digital_filters"].append(dbird_digital_filter.dbird_id())

    def __update_software_filter(self, dbird_channel, meteor_channel, meteor_channel_group):
        """
        Update software filter information of a given dbird channel according to given
        meteor information.

        dbird_channel        -- The dbird channel description
        meteor_channel       -- The meteor channel description
        meteor_channel_group -- The meteor channel group description
        """
        # WARNING: progressive AND
        if "software_filter" in meteor_channel and meteor_channel["software_filter"] != "":
            dbird_software_digital_filter = self.__get_device_component(PZAccess.SOFTWARE_DIGITAL_FILTER_METATYPE,
                                                                        {"device": [None, meteor_channel["software_filter"],
                                                                                    meteor_channel["software_filter"]],
                                                                         "serial": None})
            dbird_channel["digital_filters"].append(dbird_software_digital_filter.dbird_id())

    def __create_dbird_channel(self, meteor_channel, meteor_channel_group):
        """
        Create a new DbirdChannel according to given meteor informations

        meteor_channel       -- The meteor channel description
        meteor_channel_group -- The meteor channel group description

        return               -- The new DbirdChannel
        """
        dbird_channel = self.__initialize_dbird_channel(meteor_channel,
                                                        meteor_channel_group)
        # Manage sensors
        self.__update_sensor(dbird_channel, meteor_channel,
                             meteor_channel_group)

        # Manage external analog filter
        self.__update_analog_filter(dbird_channel, meteor_channel,
                                    meteor_channel_group)

        # Manage digitizer
        self.__update_digitizer(dbird_channel, meteor_channel,
                                meteor_channel_group)

        # Manage external digital filter
        self.__update_digital_filter(dbird_channel, meteor_channel,
                                     meteor_channel_group)

        # Manage software filter
        self.__update_software_filter(dbird_channel, meteor_channel,
                                      meteor_channel_group)

        return DbirdChannel(dbird_channel)

    def __create_channel_group(self, definition):
        """
        Create new DbirdChannel for this DbirdStation according
        to the given channel group definition.

        definition -- The channel group definition
        """
        # First we fix channel close date in definition
        if "close" not in definition or definition["close"] == "":
            definition["close"] = None
        for current_channel in definition["channel"]:
            self.__channels.append(self.__create_dbird_channel(current_channel,
                                                               definition))

    def iris_code(self):
        """
        Givce the IRIS code of this DbirdStation
        """
        return self.__definition["iris_code"]

    def first_install(self):
        """
        Give the first install date of this DbirdStation

        return -- The datetime as string in ISO format
        """
        return self.__definition["first_install"]

    def close_date(self):
        """
        Give the close date of this DbirdStation

        return -- datetime.datetime
        """
        return self.__definition.get("close", "present")

    def latitude(self):
        """
        Give the latitude of this DbirdStation

        return -- A list like (<latitude value>, <latitude min error>,
                                <latitude plus error>)
        """
        min_err = float(self.__definition["lat_min_err"]) if "lat_min_err" in self.__definition else "none"
        max_err = float(self.__definition["lat_max_err"]) if "lat_max_err" in self.__definition else "none"
        return list([float(self.__definition["lat_value"]), min_err, max_err])

    def longitude(self):
        """
        Give the longitude of this DbirdStation

        return -- A list like (<longitude value>, <longitude min error>,
                                <longitude plus error>)
        """
        min_err = float(self.__definition["lon_min_err"]) if "lon_min_err" in self.__definition else "none"
        max_err = float(self.__definition["lon_max_err"]) if "lon_max_err" in self.__definition else "none"
        return list([float(self.__definition["lon_value"]), min_err, max_err])

    def altitude(self):
        """
        Give the altitude of this DbirdStation

        return -- A list like (<altitude value>, <altitude min error>,
                                <altitude plus error>)
        """
        min_err = float(self.__definition["alt_min_err"]) if "alt_min_err" in self.__definition else "none"
        max_err = float(self.__definition["alt_max_err"]) if "alt_max_err" in self.__definition else "none"
        return list([float(self.__definition["alt_value"]), min_err, max_err])

    def toponyme(self):
        """
        The toponyme of this DbirdStation
        """
        return self.__definition.get("toponyme", "")

    def contact(self):
        """
        The contact associated with this DbirdStation
        """
        return ""

    def owner(self):
        """
        The owner associated with this DbirdStation
        """
        return ""

    def comments(self):
        """
        Give the list of DbirdComment of this DbirdStation
        """
        return self.__comments

    def locations(self):
        """
        Give the list of DbirdLocation of this DbirdStation
        """
        return self.__locations

    def sensors(self):
        """
        Give the list of DbirdSensorComponent of this DbirdStation
        """
        return self.__sensors

    def analog_filters(self):
        """
        Give the list of DbirdAnalogFilterComponent of this DbirdStation
        """
        return self.__analog_filters

    def digitizers(self):
        """
        Give the list of DbirdDigitizerComponent of this DbirdStation
        """
        return self.__digitizers

    def digital_filters(self):
        """
        Give the list of DbirdDigitalFilterComponent of this DbirdStation
        """
        return self.__digital_filters

    def channels(self):
        """
        Give the list of DbirdChannel of this DbirdStation
        """
        return self.__channels
