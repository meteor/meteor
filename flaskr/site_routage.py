#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import Flask, request, render_template, Response, abort
from pzloader.sensorlistparser import SensorListParser
from oca.meteor.validate import JsonMeteorValidator
from oca.jinjautils.filters import empty
from oca.meteor.dbird import Dbird
from pzbank.loader import PZAccess
from matplotlib import figure
from StringIO import StringIO
from tempfile import mkstemp
import obspy
import json
import os


def get_sensor_def(sensorlist_access, sensor_name):
    for current in sensorlist_access:
        result = current.get_sensor_def(sensor_name)
        if result is not None:
            return result
    return None


def load_sensor_def(pzdatabank_path_list):
    result = list()
    for current_path in pzdatabank_path_list:
        sensorlist_path = os.path.join(current_path, "sensor_list")
        if os.path.exists(sensorlist_path):
            sensor_list_file = open(sensorlist_path)
            sensor_list_content = sensor_list_file.readlines()
            current_sensor_parser = SensorListParser()
            current_sensor_parser.parse(sensor_list_content)
            result.append(current_sensor_parser)
    return result


app = Flask(__name__)
app.jinja_env.tests["empty"] = empty
app.debug = True

config_file = open("/etc/meteor/config.json")
config = json.load(config_file)

PZDATABANK_PATH = config["pzdatabank"]
PZACCESS = PZAccess(PZDATABANK_PATH)
PZACCESS.load_database()
SENSORLIST_ACCESS = load_sensor_def(PZDATABANK_PATH)

WDATALESS = config["wdataless"]
GENXML = config["genxml"]


json_validator = JsonMeteorValidator()

if "PZ" not in os.environ:
    os.environ['PZ'] = ":".join(PZDATABANK_PATH)

if "PYTHONPATH" not in os.environ:
    os.environ['PYTHONPATH'] = ':'.join(config['PYTHONPATH'])


def json_formatter(obj):
    if isinstance(obj, set):
        return list(obj)
    return obj
#
# def assign(*args):
#     args = list(args)
#     args.reverse()
#     i = 1
#     for obj in args[1:]:
#         prev = args[i - 1]
#         for k, v in prev.iteritems():
#             obj[k] = v
#         i += 1
#     return args[-1]
#
#
# def get_float(obj, key, default):
#     result = obj.get(key, default)
#     if result != default:
#         result = float(result)
#     return result
#
#
# class ItemAssignmentClass(object):
#     def __setitem__(self, k, v):
#         setattr(self, k, v)
#
#
# class Dbird(ItemAssignmentClass):
#     def __init__(self):
#         self.author = []
#         self.network = []
#         self.station = []
#         self.__net_count = 0
#         self.__net_id_mapping = {}
#
#     def add_network(self, id, values):
#         dbird_id = 'NET%s' % self.__net_count
#         self.__net_id_mapping[id] = dbird_id
#         values['dbird_id'] = dbird_id
#         # progressive OR
#         if 'close' not in values or values['close'] == '':
#             values['close'] = 'present'
#         # TODO: handle comments
#         values['comment'] = []
#         self.network.append(values)
#         self.__net_count += 1
#
#     def add_station(self, sta):
#         for ch in sta.channel:
#             ch.dbird_network = self.__net_id_mapping[ch.network]
#         self.station.append(sta)
#
#     class Station(ItemAssignmentClass):
#         def __init__(self, values):
#             # progressive OR
#             if 'close' not in values or values['close'] == '':
#                 values['close'] = 'present'
#             assign(self, values)
#             self.lat_value = float(self.lat_value)
#             self.lon_value = float(self.lon_value)
#             self.alt_value = float(self.alt_value)
#             self.lat_min_err = get_float(values, 'lat_min_err', 'none')
#             self.lat_max_err = get_float(values, 'lat_max_err', 'none')
#             self.lon_min_err = get_float(values, 'lon_min_err', 'none')
#             self.lon_max_err = get_float(values, 'lon_max_err', 'none')
#             self.alt_min_err = get_float(values, 'alt_min_err', 'none')
#             self.alt_max_err = get_float(values, 'alt_max_err', 'none')
#
#             self.location = []
#             self.__loc_id_mapping = {}
#
#             self.sensor = []
#             self.analog_filter = []
#             self.digitizer = []
#             self.digital_filter = []
#
#             self.channel = []
#
#             self.comment = []
#
#         def add_location(self, id, values):
#             dbird_id = 'LOC%s' % len(self.location)
#             self.__loc_id_mapping[id] = dbird_id
#             values['dbird_id'] = dbird_id
#             values['depth'] = float(values['depth'])
#             values['lat_value'] = float(values['lat_value'])
#             values['lon_value'] = float(values['lon_value'])
#             values['alt_value'] = float(values['alt_value'])
#             values['lat_min_err'] = get_float(values, 'lat_min_err', 'none')
#             values['lat_max_err'] = get_float(values, 'lat_max_err', 'none')
#             values['lon_min_err'] = get_float(values, 'lon_min_err', 'none')
#             values['lon_max_err'] = get_float(values, 'lon_max_err', 'none')
#             values['alt_min_err'] = get_float(values, 'alt_min_err', 'none')
#             values['alt_max_err'] = get_float(values, 'alt_max_err', 'none')
#             self.location.append(values)
#
#         def add_channel(self, ch):
#             ch.location = self.__loc_id_mapping[ch.location]
#             self.channel.append(ch)
#
#         def add_analog_filter(self, ch, **kwargs):
#             dbird_id = 'AF%s' % len(self.analog_filter)
#             ch.add_analog_filter(dbird_id)
#             config, man, mod, sn = kwargs['configuration'], kwargs['manufacturer'], kwargs['model'], kwargs['serial']
#             start = end = None
#             if sn == PZAccess.THEORETICAL:
#                 sn = None
#             else:
#                 start = PZAccess.str2date(ch.open)
#                 end = PZAccess.str2date(ch.close)
#             self.analog_filter.append({
#                 'dbird_id': dbird_id,
#                 'model': mod,
#                 'serial': 'unknown' if sn is None else sn,
#                 'pz_file': PZACCESS.get_pz_file(PZAccess.ANALOG_FILTER_METATYPE,
#                                                 man, mod, config, sn,
#                                                 kwargs['component'], start, end)
#             })
#
#         def add_digital_filter(self, ch, software_filter=False, **kwargs):
#             dbird_id = 'DF%s' % len(self.digital_filter)
#             ch.add_digital_filter(dbird_id)
#             config, man, mod, sn = kwargs['configuration'], kwargs['manufacturer'], kwargs['model'], kwargs['serial']
#             matatype = PZAccess.SOFTWARE_DIGITAL_FILTER_METATYPE if software_filter else PZAccess.DIGITAL_FILTER_METATYPE
#             self.digital_filter.append({
#                 'dbird_id': dbird_id,
#                 'model': mod,
#                 'serial': 'unknown' if sn is None else sn,
#                 'pz_file': PZACCESS.get_pz_file(matatype,
#                                                 man, mod, config)
#             })
#
#         def add_sensor(self, ch, digital_sensor=False, **kwargs):
#             dbird_id = 'S%s' % len(self.sensor)
#             ch.set_sensor(dbird_id)
#             attrs, man, mod, sn = kwargs['attrs'], kwargs['manufacturer'], kwargs['model'], kwargs['serial']
#             key_config = 'digital_sensor_sensor_configuration' if digital_sensor else 'sensor_configuration'
#             key_comp = 'digital_sensor_sensor_component' if digital_sensor else 'sensor_component'
#             key_af = 'digital_sensor_sensor_analog_filter' if digital_sensor else 'sensor_analog_filter'
#             config = attrs.get(key_config)
#             start = end = None
#             if sn == PZAccess.THEORETICAL:
#                 sn = None
#             else:
#                 start = PZAccess.str2date(ch.open)
#                 end = PZAccess.str2date(ch.close)
#             self.sensor.append({
#                 'dbird_id': dbird_id,
#                 'model': mod,
#                 'serial': 'unknown' if sn is None else sn,
#                 'azimuth': float(attrs['azimuth']),
#                 'dip': float(attrs['dip']),
#                 'pz_file': PZACCESS.get_pz_file(PZAccess.SENSOR_METATYPE,
#                                                 man, mod, config, sn,
#                                                 attrs[key_comp], start, end)
#             })
#
#             if key_af in kwargs['attrs']:
#                 self.add_analog_filter(
#                     ch, manufacturer=man, model=mod, serial=sn,
#                     configuration=attrs[key_af], component=attrs[key_comp]
#                 )
#
#         def add_digitizer(self, ch, digital_sensor=False, **kwargs):
#             dbird_id = 'D%s' % len(self.digitizer)
#             ch.set_digitizer(dbird_id)
#             attrs, man, mod, sn = kwargs['attrs'], kwargs['manufacturer'], kwargs['model'], kwargs['serial']
#             key_config = 'digital_sensor_digitizer_configuration' if digital_sensor else 'digitizer_configuration'
#             key_comp = 'digital_sensor_digitizer_component' if digital_sensor else 'digitizer_component'
#             key_af = 'digital_sensor_digitizer_analog_filter' if digital_sensor else 'digitizer_analog_filter'
#             key_df = 'digital_sensor_digital_filter' if digital_sensor else 'digitizer_digital_filter'
#             config = attrs.get(key_config)
#             start = end = None
#             if sn == PZAccess.THEORETICAL:
#                 sn = None
#             else:
#                 start = PZAccess.str2date(ch.open)
#                 end = PZAccess.str2date(ch.close)
#             self.digitizer.append({
#                 'dbird_id': dbird_id,
#                 'model': mod,
#                 'serial': 'unknown' if sn is None else sn,
#                 'pz_file': PZACCESS.get_pz_file(PZAccess.DIGITIZER_METATYPE,
#                                                 man, mod, config, sn,
#                                                 attrs[key_comp], start, end)
#             })
#
#             if key_df in kwargs['attrs']:
#                 self.add_digital_filter(
#                     ch, manufacturer=man, model=mod, serial=sn, configuration=attrs[key_df]
#                 )
#
#             if key_af in kwargs['attrs']:
#                 self.add_analog_filter(
#                     ch, manufacturer=man, model=mod, serial=sn,
#                     configuration=attrs[key_af], component=attrs[key_comp]
#                 )
#
#     class Channel(ItemAssignmentClass):
#         def __init__(self, **kwargs):
#             # progressive OR
#             if 'close' not in kwargs or kwargs['close'] == '':
#                 kwargs['close'] = 'present'
#             assign(self, kwargs)
#             self.sensor = None
#             self.analog_filter = []
#             self.digitizer = None
#             self.digital_filter = []
#             self.comment = []
#
#         def set_sensor(self, sensor):
#             self.sensor = sensor
#
#         def add_analog_filter(self, analog_filter):
#             self.analog_filter.append(analog_filter)
#
#         def set_digitizer(self, digitizer):
#             self.digitizer = digitizer
#
#         def add_digital_filter(self, digital_filter):
#             self.digital_filter.append(digital_filter)
#
#
# def convert_meteor_struct(struct):
#
#     dbird = Dbird()
#
#     # convert networks
#     for net in struct[0]['children']:
#         dbird.add_network(net['id'], net['values'])
#     for sta in struct[1]['children']:
#         # convert stations
#         dbird_sta = Dbird.Station(sta['values'])
#         for loc in sta['children'][0]['children']:
#             # convert locations
#             dbird_sta.add_location(loc['id'], loc['values'])
#         for cha in sta['children'][1]['children']:
#             # process devices
#             devs = cha['values']['device']
#             s_defined = len(filter(lambda x: x['device'][0] == 'sensor', devs)) > 0
#             d_defined = len(filter(lambda x: x['device'][0] == 'digitizer', devs)) > 0
#
#             for ch in cha['values']['channel']:
#                 values = cha['values']
#                 # progressive OR
#                 if 'close' not in values or values['close'] == '':
#                     values['close'] = 'present'
#
#                 dbird_ch = Dbird.Channel(
#                     network=values['network'], location=values['location'],
#                     iris_code=ch['iris_code'], format=values['format'],
#                     open=values['open'], close=values['close'],
#                     flag=values['flags']
#                 )
#                 dbird_sta.add_channel(dbird_ch)
#                 for dev in devs:
#                     dev_type, man, mod = dev['device']
#
#                     if dev_type == 'digital_sensor':
#                         if not s_defined:
#                             # assert sensor is plugged in an external digitizer (use only sensor part)
#                             dbird_sta.add_sensor(
#                                 dbird_ch, digital_sensor=True, manufacturer=man,
#                                 model=mod, serial=dev['serial'], attrs=ch
#                             )
#                         if not d_defined:
#                             # assert external sensor is plugged in digital sensor (use only digitizer part)
#                             dbird_sta.add_digitizer(
#                                 dbird_ch, digital_sensor=True, manufacturer=man,
#                                 model=mod, serial=dev['serial'], attrs=ch
#                             )
#                     else:
#                         # process all other devices
#                         if dev_type == 'sensor':
#                             dbird_sta.add_sensor(
#                                 dbird_ch, manufacturer=man, model=mod,
#                                 serial=dev['serial'], attrs=ch
#                             )
#                         elif dev_type == 'analog_filter':
#                             dbird_sta.add_analog_filter(
#                                 dbird_ch, manufacturer=man, model=mod,
#                                 serial=dev['serial'], configuration=None,
#                                 component=ch['analog_filter_component']
#                             )
#                         elif dev_type == 'digitizer':
#                             dbird_sta.add_digitizer(
#                                 dbird_ch, manufacturer=man, model=mod,
#                                 serial=dev['serial'], attrs=ch
#                             )
#                         elif dev_type == 'digital_filter':
#                             dbird_sta.add_digital_filter(
#                                 dbird_ch, manufacturer=man, model=mod,
#                                 serial=None, configuration=ch['digital_filter_filter']
#                             )
#                 sf = ch['software_filter']
#                 if sf != '':
#                     dbird_sta.add_digital_filter(dbird_ch, software_filter=True,
#                                                  manufacturer=sf, model=sf,
#                                                  serial=None, configuration=None)
#         dbird.add_station(dbird_sta)
#     return dbird


@app.route('/')
def index():
    return render_template('app.html')


def gen_dataless(dbird_str):
    """
    Generate Seed dataless according to a given dbird

    dbird_str -- The dbird as string

    return    -- The Seed dataless as string
    """
    fd_dbird, dbird_name = mkstemp()
    fd_dataless, dataless_name = mkstemp()
    os.close(fd_dataless)
    os.write(fd_dbird, dbird_str)
    os.close(fd_dbird)
    ret_code = os.system('%s --keep-channel-name -r %s -o %s' % (WDATALESS, dbird_name, dataless_name))
    if ret_code != 0:
        abort(500)
    f = open(dataless_name, 'r')
    os.remove(dbird_name)
    os.remove(dataless_name)
    return f.read()


def gen_stationxml(dbird_str):
    """
    Generate StationXML according to a given dbird

    dbird_str -- The dbird as string

    return    -- The StationXML as string
    """
    fd_dbird, dbird_name = mkstemp()
    fd_stationxml, stationxml_name = mkstemp()
    os.close(fd_stationxml)
    os.write(fd_dbird, dbird_str)
    os.close(fd_dbird)
    cmd = ('%(genxml)s -i %(input)s -o %(output)s --sensor %(pz)s/sensor_list --unit %(pz)s/unit_list --pz %(pz)s' %
           {'genxml': GENXML, 'input': dbird_name, 'output': stationxml_name, 'pz': ",".join(PZDATABANK_PATH)})
    ret_code = os.system(cmd)
    if ret_code != 0:
        abort(500)
    f = open(stationxml_name, 'r')
    os.remove(dbird_name)
    os.remove(stationxml_name)
    return f.read()


@app.route('/metadata/<format>', methods=['POST'])
def gen_metadata(format):
    struct = request.get_json()
    # dbird = convert_meteor_struct(struct)

    # Check Json meteor
    try:
        json_validator.check_json_meteor(struct)
    except Exception as exception:
        print(exception)
        abort(500)

    # Generate Dbird as string
    dbird = Dbird(struct, PZACCESS)
    dbird_str = render_template('dbird.tmpl', dbird=dbird)

    # Process request according to format
    if format == 'dbird':
        return dbird_str
    elif format == 'dataless':
        try:
            return Response(gen_dataless(dbird_str), mimetype="application/octet-stream")
        except Exception as exception:
            abort(500)
    elif format == 'stationxml':
        try:
            return Response(gen_stationxml(dbird_str), mimetype="application/xml")
        except Exception as exception:
            abort(500)
    else:
        abort(501)


@app.route('/plot_response', methods=['POST'])
def plot_response():
    seedids = request.args.get('seedids')
    resp_time = request.args.get('time')
    resp_type = request.args.get('type')
    if not (seedids or resp_time or resp_type):
        abort(400)
    struct = request.get_json()
    # Check Json meteor
    try:
        json_validator.check_json_meteor(struct)
    except Exception as exception:
        print(exception)
        abort(500)

    # Generate Dbird as string
    dbird = Dbird(struct, PZACCESS)
    dbird_str = render_template('dbird.tmpl', dbird=dbird)
    try:
        result = {'gain': {}, 'phase': {}}
        fake_file = StringIO(gen_stationxml(dbird_str))
        inv = obspy.read_inventory(fake_file)
        for seedid in seedids.split(','):
            tmp_fig = figure.Figure()
            ax1 = tmp_fig.add_axes([0, 0, 100, 100])
            ax2 = tmp_fig.add_axes([100, 0, 100, 100])
            resp = inv.get_response(seedid, obspy.UTCDateTime(resp_time))
            resp.plot(0.001, resp_type, axes=(ax1, ax2), show=False)
            g_data = zip(*ax1.get_lines()[0].get_data())
            p_data = zip(*ax2.get_lines()[0].get_data())
            result['gain'][seedid] = g_data[1:100] + g_data[100:1000:10] + g_data[1000::100]
            result['phase'][seedid] = p_data[1:100] + p_data[100:1000:10] + p_data[1000::100]
    except Exception as exception:
        print(exception)
        abort(500)
    return Response(json.dumps(result), mimetype='application/json')


@app.route('/ws')
def ws_usage():
    return render_template('usage.html', PZAccess=PZAccess)


@app.route('/ws/reload')
def reload():
    try:
        global PZACCESS
        PZACCESS = PZAccess(PZDATABANK_PATH)
        result = "Reload OK"
    except Exception as exception:
        result = "Error: %s" % exception
        print(result)
    return Response(json.dumps(result), mimetype='application/json')


@app.route('/ws/pz_content')
def pz_content():
    content = PZACCESS.get_pz_content_description()
    for manufacturer, manufacturer_dict in content["sensor"].iteritems():
        to_suppress = list()
        for current_sensor_model, model_dict in manufacturer_dict.iteritems():
            sensor_def = get_sensor_def(SENSORLIST_ACCESS, current_sensor_model.upper())
            if sensor_def is None:
                print("Can't find sensor definition of %s (%s)" % (current_sensor_model, manufacturer))
                to_suppress.append(current_sensor_model)
                continue
            model_dict.update(sensor_def)
        for current in to_suppress:
            del manufacturer_dict[current]
        to_suppress = list()
    return Response(json.dumps(content, indent=2, default=json_formatter), mimetype='application/json')


@app.route('/ws/pz_file', methods=['GET', 'POST'])
def pz_file():
    # TODO : Manage exception and produce nice error page
    if request.method == 'GET':
        args = request.args
    elif request.method == 'POST':
        args = request.form
    try:
        metatype = args['metatype']
        if metatype in [PZAccess.SENSOR_METATYPE, PZAccess.DIGITIZER_METATYPE,
                        PZAccess.ANALOG_FILTER_METATYPE, PZAccess.DIGITAL_FILTER_METATYPE]:
            manufacturer = args['manufacturer']
            model = args['model']
            configuration = args['configuration']
            serial = args['serial']
            component = args['component']
            if serial == PZAccess.THEORETICAL:
                result = PZACCESS.get_pz_file(metatype, manufacturer, model,
                                              configuration, None, component)
            else:
                start_date = PZAccess.str2date(args['start_date'])
                end_date = PZAccess.str2date(args['end_date'])
                result = PZACCESS.get_pz_file(metatype, manufacturer, model,
                                              configuration, serial, component,
                                              start_date, end_date)
        elif metatype == PZACCESS.DIGITAL_FILTER_METATYPE:
            manufacturer = args['manufacturer']
            model = args['model']
            configuration = args['configuration']
            result = PZACCESS.get_pz_file(metatype, manufacturer, model,
                                          configuration)
        elif metatype == PZAccess.SOFTWARE_DIGITAL_FILTER_METATYPE:
            filter_name = args['filter_name']
            result = PZACCESS.get_pz_file(metatype, filter_name, None)
        else:
            abort(400)
    except Exception as exception:
        result = "Error: %s" % exception
        print(result)
    return Response(json.dumps(result), mimetype='application/json')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)
